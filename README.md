# LemonLDAP::NG

LemonLDAP::NG is a modular Web-SSO based on Apache::Session modules.

You can find documentation here:
 * for administrators: https://lemonldap-ng.org/
 * for developers: see also embedded perldoc

# Upgrade

See https://lemonldap-ng.org/documentation/latest/upgrade

# License and copyright

LemonLDAP::NG is a free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.

Copyright: see COPYING

