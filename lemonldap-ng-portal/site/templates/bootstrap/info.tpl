<TMPL_INCLUDE NAME="header.tpl">

<div id="logincontent" class="container">

  <form id="form" action="<TMPL_VAR NAME="URL">" method="<TMPL_VAR NAME="FORM_METHOD">" class="info" role="form">
    <TMPL_VAR NAME="HIDDEN_INPUTS">
    <TMPL_IF NAME="CHOICE_VALUE">
      <input type="hidden" id="authKey" name="<TMPL_VAR NAME="CHOICE_PARAM">" value="<TMPL_VAR NAME="CHOICE_VALUE">" />
    </TMPL_IF>
    <input type="hidden" name="skin" value="<TMPL_VAR NAME="SKIN">" />
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" trspan="info">Information</h3>
      </div>
      <div class="panel-body">
        <TMPL_VAR NAME="MSG">
      </div>
    </div>
    <div class="alert alert-info">
      <p id="timer" trspan="redirectedIn">You'll be redirected in 30 seconds</p>
    </div>
      <div class="buttons">
        <button type="submit" class="positive btn btn-success">
          <span class="glyphicon glyphicon-ok"></span>
          <span trspan="continue">Continue</span>
        </button>
        <button id="wait" type="reset" class="negative btn btn-danger">
          <span class="glyphicon glyphicon-stop"></span>
          <span trspan="wait">Wait</span>
        </button>
      </div>
  </form>
  <!-- //if:jsminified
    <script type="text/javascript" src="<TMPL_VAR NAME="STATIC_PREFIX">common/js/info.min.js"></script>
  //else -->
    <script type="text/javascript" src="<TMPL_VAR NAME="STATIC_PREFIX">common/js/info.js"></script>
  <!-- //endif -->

</div>

<TMPL_INCLUDE NAME="footer.tpl">
